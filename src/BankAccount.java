public class BankAccount {
       private int balance=0; // private = restricted access

       // Getter
       public int getBalance() {
              System.out.println("Your current balance is $" + balance);
              return balance;
       }
       // Setter
       public void setBalance(int balance) {
              if ((this.balance + balance) >= 0) {
                     this.balance += balance;
                     System.out.println("Your balance has changed by " + balance + " and now it is: " + this.balance);
              } else {
                     System.out.println("Withdrawal of " + balance + " cannot be completed. Your balance is " + this.balance);
              }
       }
       public void chkTotalBalance(int totalBalance) {
              if (totalBalance == 525) {
                     System.out.println("balance is correct");
              }
       }

}